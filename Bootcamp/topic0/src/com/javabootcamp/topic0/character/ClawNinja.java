package com.javabootcamp.topic0.character;

public class ClawNinja extends Khajiit implements Warrior, Wizard {
    private String name;

    public ClawNinja(String name) {
        this.name = name;
    }
    @Override
    public String attack() {
        return "Sneak claw attack!";
    }

    @Override
    public String defend() {
        return "Khajiit mercenary defense technique!";
    }

    @Override
    public String castSpell() {
        return "Ninja art: Shadow Clone jutsu!";
    }

    @Override
    public String block() {
        return "Armguard block!";
    }

    @Override
    public String parry() {
        return "Ninja art: Redirection Jutsu!";
    }

    @Override
    public String toString() {
        return "ClawNinja " + this.name + "\r\n" +
                "Mastery skill: " + this.getSkill() + "\r\n" +
                "Attack: " + this.attack() + "\r\n" +
                "Defend: " + this.defend() + "\r\n" +
                "Cast Spell: " + this.castSpell() + "\r\n" +
                "Block: " + this.block() + "\r\n" +
                "Parry: " + this.parry() + "\r\n";
    }
}
