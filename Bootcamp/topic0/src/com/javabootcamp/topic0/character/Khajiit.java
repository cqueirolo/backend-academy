package com.javabootcamp.topic0.character;

public abstract class Khajiit implements Bipedal{
    private String skill = "Noiseless retribution";

    public String getSkill() {
        return this.skill;
    }
    @Override
    public String walk() {
        return null;
    }

    @Override
    public String run() {
        return null;
    }
}
