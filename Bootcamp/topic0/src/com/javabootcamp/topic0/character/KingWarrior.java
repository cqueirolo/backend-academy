package com.javabootcamp.topic0.character;

public class KingWarrior extends Nordic implements Warrior, Wizard {
    private String name;

    public KingWarrior(String name) {
        this.name = name;
    }

    @Override
    public String attack() {
        return "Heavy two handed sword overhead smash!";
    }

    @Override
    public String defend() {
        return "Exhiled magic shield!";
    }

    @Override
    public String castSpell() {
        return "Chaotic fire burst: Revenge of the fallen heroes!";
    }

    @Override
    public String block() {
        return "Sword block!";
    }

    @Override
    public String parry() {
        return "Sword hilt water flow attack redirection!";
    }

    @Override
    public String toString() {
        return "KingWarrior " + this.name + "\r\n" +
                "Mastery skill: " + this.getSkill() + "\r\n" +
                "Attack: " + this.attack() + "\r\n" +
                "Defend: " + this.defend() + "\r\n" +
                "Cast Spell: " + this.castSpell() + "\r\n" +
                "Block: " + this.block() + "\r\n" +
                "Parry: " + this.parry() + "\r\n";
    }
}
