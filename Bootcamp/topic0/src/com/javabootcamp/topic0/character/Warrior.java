package com.javabootcamp.topic0.character;

public interface Warrior {
    String attack();
    String block();
    String parry();
}
