package com.javabootcamp.topic0.character;

public interface Wizard {
    String attack();
    String defend();
    String castSpell();
}
