package com.javabootcamp.topic0.character;

public abstract class Nordic extends Human {
    private String skill = "Iceborn descendant";

    public String getSkill() {
        return  this.skill;
    }

    @Override
    public String walk() {
        return null;
    }

    @Override
    public String run() {
        return null;
    }
}
