package com.javabootcamp.topic0.character;

public interface Bipedal {
    String walk();
    String run();
}
