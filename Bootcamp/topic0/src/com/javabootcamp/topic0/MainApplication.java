package com.javabootcamp.topic0;

import com.javabootcamp.topic0.character.ClawNinja;
import com.javabootcamp.topic0.character.KingWarrior;

import java.util.LinkedList;
import java.util.List;

public class MainApplication {
    public static void main(String[] args) {
        System.out.println("Program started!");
        List<Object> warriors = new LinkedList<Object>();
        warriors.add(new KingWarrior("Arslan"));
        warriors.add(new ClawNinja("Noor'sah"));
        warriors.forEach(warrior -> System.out.println(warrior));
    }
}
