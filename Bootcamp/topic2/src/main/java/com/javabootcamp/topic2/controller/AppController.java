package com.javabootcamp.topic2.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AppController {
    @PostMapping("/app/interact")
    public String postResponse() {
        return "You did a POST request and THIS is the response!";
    }
    @GetMapping("/app/interact")
    public String getResponse() {
        return "You did a GET request and THIS is the response!";
    }
}
