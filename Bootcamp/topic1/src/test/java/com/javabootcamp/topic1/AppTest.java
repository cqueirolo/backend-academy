package com.javabootcamp.topic1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    Calculator calc = new Calculator();
    @Test
    public void addOperationTest() {
        assertEquals(calc.add(2.0, 5.0), 7.0, 0.00001);
    }
    @Test
    public void subtractOperationTest() {
        assertEquals(calc.subtract(6.0, 3.0), 3.0, 0.00001);
    }
}
