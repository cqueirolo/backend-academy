package com.desafio.persona.controller;

import com.desafio.persona.entity.Person;
import com.desafio.persona.service.PersonService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PersonController {

    private final PersonService personService;

    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @PostMapping("/addperson")
    private Person addPerson(@RequestBody Person person) {
        return personService.savePerson(person);
    }

    @PostMapping("/addpersons")
    private List<Person> addPersons(@RequestBody List<Person> persons) {
        return personService.savePersons(persons);
    }

    @GetMapping("/findperson/id/{id}")
    private Person findPersonById(@PathVariable long id) {
        return personService.getPersonById(id);
    }

    @GetMapping("/findperson/dni/{dni}")
    private Person findPersonByDni(@PathVariable int dni) {
        return personService.getPersonByDni(dni);
    }

    @GetMapping("/findpersons/firstname/{firstName}")
    private List<Person> findPersonsByFirstName(@PathVariable String firstName) {
        return personService.getPersonsByFirstName(firstName);
    }
    @GetMapping("/findpersons/middlename/{middleName}")
    private List<Person> findPersonsByMiddleName(@PathVariable String middleName) {
        return personService.getPersonsByMiddleName(middleName);
    }
    @GetMapping("/findpersons/lastname/{lastName}")
    private List<Person> findPersonsByLastName(@PathVariable String lastName) {
        return personService.getPersonsByLastName(lastName);
    }

    @DeleteMapping("/deleteperson/{id}")
    private String deletePerson(@PathVariable long id) {
        return personService.deletePerson(id);
    }

    @PutMapping("/updateperson")
    private Person updatePerson(@RequestBody Person person) {
        return personService.updatePerson(person);
    }
}
