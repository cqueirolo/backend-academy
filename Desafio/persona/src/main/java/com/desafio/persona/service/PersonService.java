package com.desafio.persona.service;

import com.desafio.persona.entity.Person;
import com.desafio.persona.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonService {

    private final PersonRepository personRepository;

    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public Person savePerson(Person person) {
        return personRepository.save(person);
    }
    public List<Person> savePersons(List<Person> persons) {
        return personRepository.saveAll(persons);
    }

    public Person getPersonById(long id) {
        return personRepository.findById(id).orElse(null);
    }
    public Person getPersonByDni(int dni){
        return personRepository.findByDni(dni);
    }
    public List<Person> getPersonsByFirstName(String firstName) {
        return personRepository.findAllByFirstName(firstName);
    }
    public List<Person> getPersonsByMiddleName(String middleName) {
        return personRepository.findAllByMiddleName(middleName);
    }
    public List<Person> getPersonsByLastName(String lastName) {
        return personRepository.findAllByLastName(lastName);
    }

    public Person updatePerson(Person person) {
        Person existingPerson = personRepository.findByDni(person.getDni());
        existingPerson.setFirstName(person.getFirstName());
        existingPerson.setMiddleName(person.getMiddleName());
        existingPerson.setLastName(person.getLastName());
        existingPerson.setAge(person.getAge());
        existingPerson.setHeight(person.getHeight());
        existingPerson.setAddress(person.getAddress());
        return personRepository.save(existingPerson);
    }

    public String deletePerson(long id) {
        personRepository.deleteById(id);
        return "Person deleted: " + id;
    }
}
