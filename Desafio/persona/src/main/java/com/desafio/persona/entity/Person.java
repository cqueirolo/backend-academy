package com.desafio.persona.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="PERSON_TBL")
public class Person {
    @Id
    @GeneratedValue
    private long id;
    @Column(name="first_name")
    private String firstName;
    private String middleName;
    private String lastName;
    private int dni;
    private byte age;
    private int height;
    private String address;
}
