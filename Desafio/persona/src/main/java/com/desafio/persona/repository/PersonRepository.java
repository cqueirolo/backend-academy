package com.desafio.persona.repository;

import com.desafio.persona.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PersonRepository extends JpaRepository<Person, Long> {
    Person findByDni(int dni);
    List<Person> findAllByFirstName(String firstName);
    List<Person> findAllByMiddleName(String middleName);
    List<Person> findAllByLastName(String lastName);
}

